<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;

function process_message($message)
{
    echo "\n--------\n";
    echo $message->body;
    echo "\n--------\n";
    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    // Send a message with the string "quit" to cancel the consumer.
    if ($message->body === 'quit') {
        $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
    }
}

function shutdown($channel, $connection)
{
    $channel->close();
    $connection->close();
}

class Rabbitmq_client extends CI_Controller
{
    private $connection;
    private $channel;

    public function __construct()
    {
        parent::__construct();
    }

    public function connect($host, $port, $user, $password, $vhost, $queue)
    {
        if (is_cli()) {
            $this->connection = new AMQPStreamConnection($host, intval($port), $user, urldecode($password), urldecode($vhost));
            $this->channel = $this->connection->channel();
            $this->channel->basic_consume($queue, '', false, false, false, false, 'process_message');
            register_shutdown_function('shutdown', $this->channel, $this->connection);
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        } else {
            show_404();
        }
    }
}
