#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect(process.argv[8] + '://' + process.argv[4] + ':' + process.argv[5] + '@' + process.argv[2] + ':' + process.argv[3] + '/' + process.argv[6], function(err, conn) {
  conn.createChannel(function(err, ch) {
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", process.argv[7]);
    ch.consume(process.argv[7], function(msg) {
      console.log(" [x] Received %s", msg.content.toString());
    }, {noAck: true});
  });
});
