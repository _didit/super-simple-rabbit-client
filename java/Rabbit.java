import com.rabbitmq.client.*;

import java.io.IOException;
import java.lang.String;
import java.lang.Integer;

public class Rabbit {
    public static void main(String[] argv) throws Exception {
        String Uri = argv[6]+ "://" + argv[2] + ":" + argv[3] + "@" + argv[0] + ":" + argv[1] + "/" + argv[4];
        System.out.println(" Connect To = " + Uri);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(Uri);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                    byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
            }
        };
        channel.basicConsume(argv[5], true, consumer);
    }
}
